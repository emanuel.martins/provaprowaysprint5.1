# 🌐 Prova Proway


<ul style="list-style-type: square;">
    <li>Sprint 5</li>
    <li>Parte 1</li>
</ul>

<code>Sistema para controle de combustível</code>
<br>

### 👤 Entidades

****
#### Produto <br>
- ADICIONAR; 
- LISTAR TODOS; 
- LISTAR PELO ID; 
- ATUALIZAR;
- DELETAR;
- LISTAR PELO VALOR;

****
#### Fornecedor <br>
- ADICIONAR;
- LISTAR TODOS;
- LISTAR PELO ID;
- ATUALIZAR;
- DELETAR;
- LISTAR ROTA;

****
#### Tanque
- ADICIONAR;
- LISTAR TODOS;
- LISTAR PELO ID;
- ATUALIZAR;
- DELETAR;
- LISTAR PELA CAPACIDADE;
- LISTAR PELO NÚMERO DE SÉRIE;

<br>

<h2 style="width: 100%; text-align: center; border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;">
       <span style="padding:0 10px; ">✨ INSTRUÇÕES 🌟</span>
</h2><br>

Para importar uma Request Collection em .json<br>
Baixe o arquivo da [Coleção](src/main/resources/templates/Posto de Combustível.postman_collection.json)
e em seu [Postman](https://postman.com) vá para a aba de
"Collections" e clique em "Import":

<img src="https://i.gyazo.com/9ee27277843d1ee1c86a247cdf9af7cb.png" alt="Demonstração da aba Collections à esquerta e o botão import" width="380"/>

Após na aba "File" clique em "Upload Files": 

<img src="https://i.gyazo.com/5bf6d67cbea8dde9fa2443351e12ec14.png" alt="Aba Files com o botão Upload Files ao centro">

Navegue até o seu arquivo baixado e logo após clique em "Import":

<img src="https://i.gyazo.com/1a25b0ea5452fd17111f09643e6c1acc.png" alt="Arquivo já selecionado com o botão import disponível">

Com isso, sua coleção irá aparecer completa ja na aba "Collections" e você terá acesso a todas as requests

****
<p align="center">Prova Sprint 5 parte 1 - Emanuel Martins</p>

<p align="center"><img src="https://c.tenor.com/ORwVOmkKdYEAAAAi/pato-caminando.gif" alt="A duck GIF"/>
