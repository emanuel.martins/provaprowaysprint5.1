package br.com.proway.prova.repositories;

import br.com.proway.prova.models.Tanque;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface TanqueRepository extends JpaRepository<Tanque, Long> {
    Tanque save(Tanque tanque);
    ArrayList<Tanque> findAll();
    Tanque findById(long id);
    void deleteById(long id);

    @Query("SELECT t FROM Tanque t WHERE t.quantidade <= :quantidade")
    ArrayList<Tanque> findTankByLimit(@Param("quantidade") Float quantidade);

    @Query("SELECT t FROM Tanque t WHERE t.numeroSerie LIKE CONCAT('%',:numeroSerie,'%')")
    Tanque findByNumber(@Param("numeroSerie") String numeroSerie);
}
