package br.com.proway.prova.repositories;


import br.com.proway.prova.models.Fornecedor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {
    Fornecedor save(Fornecedor fornecedor);
    ArrayList<Fornecedor> findAll();
    Fornecedor findById(long id);
    void deleteById(Long id);

    @Query("SELECT f FROM Fornecedor f WHERE f.descricao LIKE CONCAT('%',:descricao,'%')")
    ArrayList<Fornecedor> findByDescription(@Param("descricao") String descricao);
}
