package br.com.proway.prova.repositories;

import br.com.proway.prova.models.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    Produto save(Produto produto);
    ArrayList<Produto> findAll();
    Produto findById(long id);
    void deleteById(Long id);

    @Query("SELECT p FROM Produto p WHERE p.valor <= :valor")
    ArrayList<Produto> findByLessOrEqual(@Param("valor") Double valor);
}
