package br.com.proway.prova.exceptions;

public class BadRequestException extends Exception {
    public BadRequestException(String mensagem) {
        super(mensagem);
    }
}
