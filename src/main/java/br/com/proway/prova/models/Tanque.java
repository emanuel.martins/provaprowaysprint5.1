package br.com.proway.prova.models;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Data
@Table(name = "tanques")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Tanque {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_serie")
    private String numeroSerie;

    @Column
    private Float quantidade;
}
