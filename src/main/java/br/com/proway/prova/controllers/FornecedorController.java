package br.com.proway.prova.controllers;

import br.com.proway.prova.exceptions.BadRequestException;
import br.com.proway.prova.models.Fornecedor;
import br.com.proway.prova.repositories.FornecedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/fornecedores")
public class FornecedorController {

    @Autowired
    private FornecedorRepository repositorio;

    @PostMapping
    public ResponseEntity addFornecedor(@RequestBody Fornecedor body) {
        Fornecedor fornecedor;

        try {
            if (body.getCnpj().isEmpty()) {
                throw new BadRequestException("CNPJ não pode estar vazio");
            }

            fornecedor = repositorio.save(body);

            return new ResponseEntity<Fornecedor>(fornecedor, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            System.err.println(e.getMessage());

            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ArrayList<Fornecedor> getAllFornecedor() {
        return repositorio.findAll();
    }

    @GetMapping("/{id}")
    public Fornecedor getFornecedor(@PathVariable("id") long id) {
        return repositorio.findById(id);
    }

    @GetMapping("/rota/{descricao}")
    public ArrayList<Fornecedor> getFornecedoresLocalizacao(@PathVariable("descricao") String descricao) {
        return repositorio.findByDescription(descricao);
    }

    @PutMapping("/{id}")
    public Fornecedor atualizarProduto(@PathVariable("id") long id, @RequestBody Fornecedor body) {
        body.setId(id);

        return repositorio.save(body);
    }

    @DeleteMapping("/{id}")
    public void deleteFornecedor(@PathVariable("id") long id) {
        repositorio.deleteById(id);
    }
}
