package br.com.proway.prova.controllers;

import br.com.proway.prova.exceptions.BadRequestException;
import br.com.proway.prova.models.Produto;
import br.com.proway.prova.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoRepository repositorio;

    @PostMapping
    public ResponseEntity addProduto(@RequestBody Produto body) {
        Produto novoProduto;

        try {
            if (body.getNome().isEmpty()) {
                throw new BadRequestException("Por favor informe o nome do produto!");
            }

            novoProduto = repositorio.save(body);

            return new ResponseEntity<Produto>(novoProduto, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ArrayList<Produto> getAllProduto() {
        return repositorio.findAll();
    }

    @GetMapping("/{id}")
    public Produto getProduto(@PathVariable("id") long id) {
        return repositorio.findById(id);
    }

    @GetMapping("/valor/{valor}")
    public ArrayList<Produto> getProdutoPeloValor(@PathVariable("valor") Double valor) {
        return repositorio.findByLessOrEqual(valor);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable("id") long id, @RequestBody Produto produto){
        produto.setId(id);
        return repositorio.save(produto);
    }

    @DeleteMapping("/{id}")
    public void deleteProduto(@PathVariable("id") long id) {
        repositorio.deleteById(id);
    }
}
