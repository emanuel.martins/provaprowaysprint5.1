package br.com.proway.prova.controllers;

import br.com.proway.prova.exceptions.BadRequestException;
import br.com.proway.prova.models.Tanque;
import br.com.proway.prova.repositories.TanqueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/tanques")
public class TanqueController {

    @Autowired
    private TanqueRepository repositorio;

    @PostMapping
    public ResponseEntity addTanque(@RequestBody Tanque body) {
        Tanque novoTanque;

        try {
            if (body.getNumeroSerie().isEmpty()) {
                throw new BadRequestException("Por favor informe um Número de Série!");
            }

            novoTanque = repositorio.save(body);

            return new ResponseEntity<Tanque>(novoTanque, HttpStatus.CREATED);
        } catch (BadRequestException e) {
            System.err.println(e.getLocalizedMessage());

            return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ArrayList<Tanque> getAllTanque() {
        return repositorio.findAll();
    }

    @GetMapping("/{id}")
    public Tanque getTanque(@PathVariable("id") long id) {
        return repositorio.findById(id);
    }

    @GetMapping("/capacidade/{quantidade}")
    public ArrayList<Tanque> getTanqueByQuantidade(@PathVariable("quantidade") Float quantidade) {
        return repositorio.findTankByLimit(quantidade);
    }

    @GetMapping("/numero/{numeroSerie}")
    public Tanque getByNumeroSerie(@PathVariable("numeroSerie") String numeroSerie) {
        return repositorio.findByNumber(numeroSerie);
    }

    @PutMapping("/{id}")
    public Tanque atualizarTanque(@PathVariable("id") long id, @RequestBody Tanque body) {
        body.setId(id);
        return repositorio.save(body);
    }

    @DeleteMapping("/{id}")
    public void deleteTanque(@PathVariable long id) {
        repositorio.deleteById(id);
    }
}
