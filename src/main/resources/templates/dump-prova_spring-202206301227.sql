--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

-- Started on 2022-06-30 12:27:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 17071)
-- Name: fornecedores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fornecedores (
    id integer NOT NULL,
    cnpj character varying NOT NULL,
    cidade character varying,
    descricao character varying
);


ALTER TABLE public.fornecedores OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 17070)
-- Name: fornecedores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fornecedores_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fornecedores_id_seq OWNER TO postgres;

--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 211
-- Name: fornecedores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fornecedores_id_seq OWNED BY public.fornecedores.id;


--
-- TOC entry 210 (class 1259 OID 17062)
-- Name: produtos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produtos (
    id integer NOT NULL,
    nome character varying NOT NULL,
    valor double precision
);


ALTER TABLE public.produtos OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 17061)
-- Name: produtos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.produtos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.produtos_id_seq OWNER TO postgres;

--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 209
-- Name: produtos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.produtos_id_seq OWNED BY public.produtos.id;


--
-- TOC entry 214 (class 1259 OID 17080)
-- Name: tanques; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tanques (
    id integer NOT NULL,
    numero_serie character varying NOT NULL,
    quantidade double precision
);


ALTER TABLE public.tanques OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 17079)
-- Name: tanques_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tanques_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tanques_id_seq OWNER TO postgres;

--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 213
-- Name: tanques_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tanques_id_seq OWNED BY public.tanques.id;


--
-- TOC entry 3175 (class 2604 OID 17074)
-- Name: fornecedores id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fornecedores ALTER COLUMN id SET DEFAULT nextval('public.fornecedores_id_seq'::regclass);


--
-- TOC entry 3174 (class 2604 OID 17065)
-- Name: produtos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produtos ALTER COLUMN id SET DEFAULT nextval('public.produtos_id_seq'::regclass);


--
-- TOC entry 3176 (class 2604 OID 17083)
-- Name: tanques id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanques ALTER COLUMN id SET DEFAULT nextval('public.tanques_id_seq'::regclass);


--
-- TOC entry 3325 (class 0 OID 17071)
-- Dependencies: 212
-- Data for Name: fornecedores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fornecedores (id, cnpj, cidade, descricao) FROM stdin;
1	09114248000158	São Paulo	São Paulo para Cuiabá
2	79292422000138	São Paulo	São Paulo para Goiania
3	64499874000118	São Paulo	São Paulo para Salvador
4	57252994000150	São Luiz	São Luiz para Fortaleza
6	40391568000103	Maringá	Maringá para Pato Branco
7	40391568000103	Maringá	Maringá para Chapecó
\.


--
-- TOC entry 3323 (class 0 OID 17062)
-- Dependencies: 210
-- Data for Name: produtos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.produtos (id, nome, valor) FROM stdin;
5	Gasolina Comum	6542
4	Gasolina Aditivada	6742
6	Diesel	6918
7	Etanol	4990
8	GNV m3	4308
9	Óleo diesel	5496
\.


--
-- TOC entry 3327 (class 0 OID 17080)
-- Dependencies: 214
-- Data for Name: tanques; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tanques (id, numero_serie, quantidade) FROM stdin;
1	TNK - 0884	15000
5	TNK - 0888	15000
4	TNK - 0887	22000
2	TNK - 0885	29000
3	TNK - 0886	26500
\.


--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 211
-- Name: fornecedores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fornecedores_id_seq', 7, true);


--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 209
-- Name: produtos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.produtos_id_seq', 9, true);


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 213
-- Name: tanques_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tanques_id_seq', 6, true);


--
-- TOC entry 3180 (class 2606 OID 17078)
-- Name: fornecedores fornecedores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fornecedores
    ADD CONSTRAINT fornecedores_pkey PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 17069)
-- Name: produtos produtos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produtos
    ADD CONSTRAINT produtos_pkey PRIMARY KEY (id);


--
-- TOC entry 3182 (class 2606 OID 17087)
-- Name: tanques tanques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanques
    ADD CONSTRAINT tanques_pkey PRIMARY KEY (id);


-- Completed on 2022-06-30 12:27:32

--
-- PostgreSQL database dump complete
--

